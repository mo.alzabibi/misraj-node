@echo off
setlocal

REM Set MySQL credentials
set MYSQL_USER=root
set MYSQL_PASSWORD=
set MYSQL_HOST=127.0.0.1
set MYSQL_PORT=3306

REM Path to SQL script (relative path)
set SQL_SCRIPT="migrate.sql"

REM Change to the directory of the batch file to ensure relative paths work
cd /d %~dp0

REM Execute the SQL script and capture any errors
mysql -u %MYSQL_USER% -p%MYSQL_PASSWORD% -h %MYSQL_HOST% --port=%MYSQL_PORT% < %SQL_SCRIPT% 2>error.log

REM Check if error.log is empty
if %ERRORLEVEL% neq 0 (
    echo An error occurred. Check error.log for details.
) else (
    echo Database and tables created successfully.
)

endlocal
pause