CREATE DATABASE IF NOT EXISTS misraj;
USE misraj;

CREATE TABLE chat (
  id int NOT NULL AUTO_INCREMENT,
  conversationId varchar(255) DEFAULT NULL,
  role enum('user','bot') DEFAULT NULL,
  content text,
  timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE files (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL UNIQUE,
  publicName varchar(255) NOT NULL UNIQUE,
  createdAt timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO files (id, name, publicName, createdAt)
VALUES (1, 'test4.csv', '1716660578458.csv', '2024-05-25 18:09:38');