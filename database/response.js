// Success response handler
export function successResponse({ data, message }) {
    return {
        data: data || [],
        message: message || "Success",
    };
}

// Error response handler
export function errorResponse({ data, message, status }) {
    return {
        data: data || [],
        message: message || "Error",
        status: status || 500
    };
}