import express from 'express';
import { chatWithDialoGPT,  getChat } from '../../controllers/gpt/index.js';

const Gptrouter = express.Router();


Gptrouter.post('/chat', async (req, res) => {
    try {
        const { prompt, conversationId } = req.body;
        const response = await chatWithDialoGPT(prompt,conversationId);
        
        res.status(200).send(response);
    } catch (error) {
        res.status(500).send({ error: error.message });
    }
});
Gptrouter.get('/chat/:conversationId', async (req, res) => {
        const {conversationId } = req.params;
        const response = await getChat(conversationId);
        res.status(200).send(response);
});

export default Gptrouter;