import express from "express";
import { deleteFile, getFile, getFileStatistics, getFilesName, upload, uploadCSV } from "../../controllers/files/index.js";

const filesRouters = express.Router();

filesRouters.get("/", async (req, res, next) => {
    try {
      const files = await getFilesName();
      res.send(files);
    } catch (err) {
      res.status(500).send("Something went wrong!");
    }
  });

filesRouters.post('/',upload.single('csvData'),uploadCSV);
filesRouters.get('/:id', getFile);
filesRouters.delete('/delete/:id', deleteFile);
filesRouters.get('/statistics/:id', getFileStatistics);

export default filesRouters;
