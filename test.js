
export async function getFileDataById(id) {
    try {
        const [rows] = await pool.query("SELECT publicName FROM files WHERE id = ?", [id]);
        if (rows.length === 0) {
            throw new Error("No file found with the provided ID.");
        }
        const { publicName } = rows[0];
        const filePath = path.join('uploads', publicName);
        
        if (!fs.existsSync(filePath)) {
            throw new Error("File not found on server.");
        }

        const fileContent = fs.readFileSync(filePath, 'utf8');
        const results = papa.parse(fileContent, { header: true });
        return results.data;
    } catch (error) {
        throw new Error(`Failed to get file data: ${error.message}`);
    }
}

async function generateTextWithHf(prompt) {
    try {
        const response = await hf.textGeneration({
            model: 'gpt2',
            inputs: prompt,
            parameters: {
                max_new_tokens: 150,
                return_full_text: false,
            },
        });
        return response.generated_text.trim();
    } catch (error) {
        throw new Error(`Failed to generate text: ${error.message}`);
    }
}
function analyzeCsvData(data) {
    const analysis = {};
    const columns = Object.keys(data[0]);

    // Iterate over each column in the data
    columns.forEach(col => {
        const columnData = data.map(row => {
            // Handle possible null, undefined, or empty string values
            return row[col] !== null && row[col] !== undefined ? row[col].toString().trim() : '';
        }).filter(val => val !== ''); // Remove empty strings from analysis

        // Check if all elements in the column are numeric
        const isNumeric = columnData.every(val => !isNaN(parseFloat(val)) && isFinite(val));

        if (isNumeric) {
            const numericData = columnData.map(Number);
            analysis[col] = {
                type: 'numeric',
                mean: mean(numericData),
                median: median(numericData),
                std: std(numericData),
                max: max(numericData),
                min: min(numericData),
                sum: sum(numericData)
            };
        } else {
            const uniqueValues = _.uniq(columnData);
            const valueCounts = _.countBy(columnData);
            analysis[col] = {
                type: 'categorical',
                uniqueValues,
                valueCounts
            };
        }
    });

    return analysis;
}

function createPrompt(analysis) {
    const analysisParts = Object.keys(analysis).map(col => {
        const colAnalysis = analysis[col];
        if (colAnalysis.type === 'numeric') {
            return `Column ${col}: mean = ${colAnalysis.mean.toFixed(2)}, median = ${colAnalysis.median.toFixed(2)}, std = ${colAnalysis.std.toFixed(2)}, max = ${colAnalysis.max}, min = ${colAnalysis.min}, sum = ${colAnalysis.sum.toFixed(2)}`;
        } else {
            const valueSummary = colAnalysis.uniqueValues.map(value => {
                return `${value}: ${colAnalysis.valueCounts[value]} occurrences`;
            }).join(', ');
            const mostCommon = _.maxBy(_.keys(colAnalysis.valueCounts), o => colAnalysis.valueCounts[o]);
            return `Column ${col} (non-numeric): most common value is '${mostCommon}' with ${colAnalysis.valueCounts[mostCommon]} occurrences. Other values include ${valueSummary}`;
        }
    }).join('\n');

    return `Based on the following analysis of the CSV data, provide a summary and key insights. Explain the purpose of the data and highlight any significant patterns or trends:\n${analysisParts}`;
}

export async function processCsvWithGpt(data) {
    try {
        const analysis = analyzeCsvData(data);
        const prompt = createPrompt(analysis);
        const result = await generateTextWithHf(prompt);

        return {
            data: result,
            message: "GPT analysis completed successfully."
        };
    } catch (error) {
        throw new Error(`Failed to process CSV with GPT: ${error.message}`);
    }
}


// Gptrouter.get('/gpt/:id', async (req, res) => {
//     try {
//         const fileData = await getFileDataById(req.params.id);
//         const insights = await processCsvWithGpt(fileData);
//         res.status(200).json(successResponse({ data: insights, message: "GPT analysis completed successfully." }));
//     } catch (error) {
//         res.status(500).json(errorResponse({ message: error.message }));
//     }
// });