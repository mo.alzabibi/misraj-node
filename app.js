import express from "express";
import cors from "cors"; // Import CORS module
import filesRouters from "./routes/files/index.js"; // Ensure correct path
import Gptrouter from "./routes/gpt/index.js";

const app = express();

// Configure CORS options
const corsOptions = {
    origin: '*', // Set to appropriate origins in production
    optionsSuccessStatus: 200 
};

app.use(cors(corsOptions)); // Use CORS middleware globally
app.use(express.json()); // Middleware to parse JSON bodies

// Mount your route at the root level or under a specific path
app.use("/file", filesRouters);
app.use("/gpt", Gptrouter);

// Central error handling
app.use((err, req, res, next) => {
  res.status(500).send("Something broke!");
});

app.listen(3200, () => {
  console.log("running at http://127.0.0.1:3200")
});
