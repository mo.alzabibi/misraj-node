import pool from '../../database/index.js';
import _ from 'lodash';
import fetch from 'node-fetch';
import { errorResponse, successResponse } from '../../database/response.js';

/* Chating */
/* ------------------------------------------------------------------------------------------------------ */
const MAX_HISTORY_LENGTH = 5;  // Limit the number of messages in the context
const MAX_RETRIES = 5;  // Maximum number of retries for the API call
const RETRY_DELAY = 1000;  // Initial retry delay in milliseconds

export async function chatWithDialoGPT(prompt, conversationId) {
    let conversationHistory = await getConversationHistory(conversationId);
    
    try {
        // Add the user prompt to the conversation history
        conversationHistory.push(`User: ${prompt}`);

        // Limit the conversation history
        if (conversationHistory.length > MAX_HISTORY_LENGTH) {
            conversationHistory = conversationHistory.slice(-MAX_HISTORY_LENGTH);
        }

        // Create the context from the conversation history, with a clean format
        const context = conversationHistory.join('\n') + '\nBot:';

        const response = await fetchWithRetry('https://api-inference.huggingface.co/models/microsoft/DialoGPT-medium', {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${process.env.HUGGINGFACE_API_KEY}`,   
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                inputs: context,
                parameters: {
                    max_new_tokens: 50,
                    temperature: 0.7,
                    repetition_penalty: 1.2,
                },
            }),
        }, MAX_RETRIES, RETRY_DELAY);

        const data = await response.json();

        if (!data || !data[0] || !data[0].generated_text) {
            throw new Error('API response does not contain valid generated text');
        }

        const generatedText = data[0].generated_text;

        const botResponse = extractBotResponse(generatedText);

        if (!botResponse) {
            throw new Error('API response does not contain a valid bot response');
        }

        // Add the bot response to the conversation history
        conversationHistory.push(`Bot: ${botResponse}`);

        await storeConversation(conversationId, prompt, botResponse);

        return successResponse({ data: botResponse });
    } catch (error) {
        throw new Error(`Failed to generate text: ${error.message}`);
    }
}

async function fetchWithRetry(url, options, retries, delay) {
    for (let i = 0; i <= retries; i++) {
        const response = await fetch(url, options);
        if (response.ok) {
            return response;
        }
        if (response.status === 503) {
            const errorDetails = await response.json();
            await new Promise(res => setTimeout(res, delay));
            delay *= 2;  // Exponential backoff
        } else {
            const errorDetails = await response.json();
            throw new Error(`API Error: ${response.status} ${response.statusText} - ${JSON.stringify(errorDetails)}`);
        }
    }
    throw new Error('Max retries reached. Service is unavailable.');
}

function extractBotResponse(generatedText) {
    // Extract the bot response after the last 'Bot:' in the generated text
    const botResponses = generatedText.split('\n').filter(line => line.startsWith('Bot:'));
    const botResponse = botResponses.length > 0 ? botResponses[botResponses.length - 1].substring(5).trim() : null;
    return botResponse;
}

async function getConversationHistory(conversationId) {
    try {
        const [rows] = await pool.query(`SELECT role, content FROM chat WHERE conversationId = ? ORDER BY id ASC`, [conversationId]);
        return rows.map(row => `${row.role.charAt(0).toUpperCase() + row.role.slice(1)}: ${row.content}`);
    } catch (error) {
        console.error('Error fetching conversation history:', error);
        return [];
    }
}

async function storeConversation(conversationId, userMessage, botMessage) {
    const query = 'INSERT INTO chat (conversationId, role, content) VALUES (?, ?, ?), (?, ?, ?)';
    const values = [
        conversationId, 'user', userMessage,
        conversationId, 'bot', botMessage
    ];

    try {
        const [results] = await pool.query(query, values);
        return results; // This returns the results of the query
    } catch (error) {
        throw error; // Rethrow the error to handle it further up the call stack
    }
}

/* ------------------------------------------------------------------------------------------------------ */
/* Get Chats */
export async function getChat(conversationId) {
    try {
        const [rows] = await pool.query(`SELECT * FROM chat WHERE conversationId = ?`, [conversationId]);
        if (rows.length) {
            return successResponse({ data: rows });
        }
        return successResponse({ data: [], message: "No conversations found" });
    } catch (error) {
        return errorResponse({ data: [], message: "Error" });
    }
}
