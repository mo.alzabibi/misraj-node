import multer from 'multer';
import fs from 'fs';
import path from 'path';
import { errorResponse, successResponse } from '../../database/response.js';
import pool from '../../database/index.js';
import papa from 'papaparse';

/* GET Files */
export async function getFilesName() {
    const [rows] = await pool.query("SELECT * FROM files");
    return successResponse({ data: rows });
}
/* ----------------------------------------------------------------------------------------------------------- */

/* Upload file */
const storage = multer.diskStorage({
    destination: 'uploads/',
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

export const upload = multer({ storage: storage });

export const uploadCSV = async (req, res) => {
    if (!req.file) {
        return res.status(400).json(errorResponse({ message: 'Please upload a CSV file!', status: 400 }));
    }

    const [check] = await pool.query(`SELECT id FROM files WHERE name = ?`, [req.file.originalname]);
    if (check.length > 0) {
        return res.status(400).json(errorResponse({ data: [], message: 'This file already exists' }));
    }

    try {
        const currentDate = new Date();
        const timestamp = currentDate.getTime();
        const newFileName = `${timestamp}${path.extname(req.file.originalname)}`;
        const newFilePath = path.join('uploads', newFileName);
        fs.renameSync(req.file.path, newFilePath);

        const query = 'INSERT INTO files (name, publicName, createdAt) VALUES (?, ?, ?)';
        await pool.query(query, [req.file.originalname, newFileName, currentDate.toISOString().slice(0, 19).replace('T', ' ')]);

        res.status(200).json(successResponse({ data: [], message: 'File uploaded and processed successfully' }));
    } catch (error) {
        res.status(400).json(errorResponse({ message: 'Failed to process file', data: error.message }));
    }
};

/* GET single file data */
/* ----------------------------------------------------------------------------------------------------------- */

export const getFile = async (req, res) => {
    const { id } = req.params;

    try {
        const [rows] = await pool.query("SELECT publicName FROM files WHERE id = ?", [id]);
        if (rows.length === 0) {
            return res.status(404).json(errorResponse({ message: "No file found with the provided ID.", status: 404 }));
        }

        const { publicName } = rows[0];
        const filePath = path.join('uploads', publicName);

        if (!fs.existsSync(filePath)) {
            return res.status(404).json(errorResponse({ message: "File not found on server.", status: 404 }));
        }

        const fileStream = fs.createReadStream(filePath, 'utf8');
        let rowIndex = 0;
        const dataWithIds = [];

        papa.parse(fileStream, {
            header: true,
            step: (results) => {
                const data = results.data;
                dataWithIds.push({
                    id: ++rowIndex,
                    ...data
                });
            },
            complete: () => {
                res.status(200).json(successResponse({ data: dataWithIds, message: "CSV data retrieved successfully." }));
            },
            error: (error) => {
                res.status(400).json(errorResponse({ message: "Failed to parse CSV file", data: error.message }));
            },
        });
    } catch (error) {
        res.status(500).json(errorResponse({ message: "Server error", data: error.message }));
    }
};

/* Delete File */
/* ----------------------------------------------------------------------------------------------------------- */

export const deleteFile = async (req, res) => {
    const { id } = req.params;

    try {
        const [rows] = await pool.query("SELECT publicName FROM files WHERE id = ?", [id]);
        if (rows.length === 0) {
            return res.status(404).json(errorResponse({ message: "No file found with the provided ID.", status: 404 }));
        }

        const { publicName } = rows[0];
        const filePath = path.join('uploads', publicName);

        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
        }

        await pool.query("DELETE FROM files WHERE id = ?", [id]);
        res.status(200).json(successResponse({ message: "File deleted successfully." }));
    } catch (error) {
        res.status(500).json(errorResponse({ message: "Server error", data: error.message }));
    }
};
/* ----------------------------------------------------------------------------------------------------------- */
/* Get File Statistics */


export const getFileStatistics = async (req, res) => {
    const { id } = req.params;

    try {
        // Step 1: Retrieve the publicName from the database using the file ID
        const [rows] = await pool.query('SELECT * FROM files WHERE id = ?', [id]);
        if (rows.length === 0) {
            return res.status(404).json(errorResponse({ message: 'No file found with the provided ID.', status: 404 }));
        }

        const { publicName,name,createdAt } = rows[0];
        const filePath = path.join('uploads', publicName);

        // Step 2: Check if the file exists
        if (!fs.existsSync(filePath)) {
            return res.status(404).json(errorResponse({ message: 'File not found on server.', status: 404 }));
        }

        // Initialize statistics
        let numColumns = 0;
        let numRows = 0;
        const columnData = {};

        // Step 3: Stream and parse the CSV file
        const fileStream = fs.createReadStream(filePath, 'utf8');

        papa.parse(fileStream, {
            header: true,
            worker: true,
            step: (results) => {
                const data = results.data;

                if (!numColumns) {
                    numColumns = results.meta.fields.length;
                    results.meta.fields.forEach((field) => {
                        columnData[field] = {
                            count: 0,
                            uniqueValues: new Set(),
                            sum: 0,
                            numericCount: 0,
                            isIntegerColumn: true,
                            min: Infinity,
                            max: -Infinity,
                            values: [], // Store numeric values for median and std deviation calculation
                        };
                    });
                }
                numRows++;

                results.meta.fields.forEach((field) => {
                    const value = data[field];
                    const numericValue = parseFloat(value);
                    const isNumeric = !isNaN(numericValue);

                    if (isNumeric) {
                        columnData[field].sum += numericValue;
                        columnData[field].numericCount++;
                        columnData[field].values.push(numericValue); // Add value to array for later median/std deviation calculation
                        if (numericValue < columnData[field].min) {
                            columnData[field].min = numericValue;
                        }
                        if (numericValue > columnData[field].max) {
                            columnData[field].max = numericValue;
                        }
                        if (!Number.isInteger(numericValue)) {
                            columnData[field].isIntegerColumn = false;
                        }
                    } else {
                        columnData[field].isIntegerColumn = false;
                    }

                    columnData[field].count++;
                    columnData[field].uniqueValues.add(value);
                });
            },
            complete: () => {
                const columns = Object.keys(columnData).map((field) => {
                    const fieldData = columnData[field];
                    const mean = fieldData.numericCount ? fieldData.sum / fieldData.numericCount : null;
                    const uniqueValuesArray = Array.from(fieldData.uniqueValues).filter((value) => value !== null && value !== '' && value !== undefined);
                    

                    // Calculate median
                    const sortedValues = fieldData.values.sort((a, b) => a - b);
                    const middle = Math.floor(sortedValues.length / 2);
                    const median = sortedValues.length % 2 === 0 ? (sortedValues[middle - 1] + sortedValues[middle]) / 2 : sortedValues[middle];

                    // Calculate standard deviation
                    const variance = fieldData.numericCount ? sortedValues.reduce((acc, val) => acc + Math.pow(val - mean, 2), 0) / fieldData.numericCount : null;
                    const stdDev = variance !== null ? Math.sqrt(variance) : null;

                    return {
                        name: field,
                        uniqueValuesCount: uniqueValuesArray.length < 10 ? uniqueValuesArray.length : null,
                        uniqueValues: uniqueValuesArray.length < 10 ? uniqueValuesArray : null,
                        isIntegerColumn: fieldData.isIntegerColumn,
                        sum: fieldData.numericCount ? fieldData.sum : null,
                        mean: mean,
                        min: fieldData.numericCount ? fieldData.min : null,
                        max: fieldData.numericCount ? fieldData.max : null,
                        median: fieldData.numericCount ? median : null,
                        stdDev: fieldData.numericCount ? stdDev : null,
                    };
                });
                const stats = {
                    id:id,
                    name:name,
                    publicName:publicName,
                    createdAt:createdAt,
                    numColumns,
                    numRows,
                    columns,
                };

                res.status(200).json(successResponse({ data: stats, message: 'File statistics retrieved successfully.' }));
            },
            error: (error) => {
                res.status(400).json(errorResponse({ message: 'Failed to parse CSV file', data: error.message }));
            },
        });
    } catch (error) {
        res.status(500).json(errorResponse({ message: "Server error", data: error.message }));
    }
};